import { default as express } from 'express'
import { default as cors } from 'cors'
import { MongoClient } from 'mongodb'

const app = express()

app.use(cors())
app.use(express.static(__dirname+"/public"))

app.get('/signup',(req, res)=>{
  res.sendFile(__dirname+"/public/signUp.html")
})
app.post('/signup',(req, res)=>{

})
app.get('/login',(req, res)=>{
  MongoClient.connect('mongodb://[::1]/test', {useNewUrlParser: true}, (err, client) =>{
  if (err) throw err

  var db = client.db('test');
  })
})

app.get('*', (req, res)=>{
  res.status(404)
  .sendFile(__dirname+'/public/404.html');
})

app.listen(80, ()=>{
  console.log('Started server on port 80')
})